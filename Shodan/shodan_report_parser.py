import ipaddress
import json
import os
import time
import sys

shodan_json_file = r"C:\Users\Administrator\Desktop\shodan\fs2kep7fc.json"
output = r"targets.txt"

def main():
    """This script will parse JSON data exported from SHODAN and create IP:PORT formatted list to be used
        with other tools. To run specify path to a file with JSON data from SHODAN."""

    if not os.path.exists(shodan_json_file):
        print('\nError: Provided input file does not exist')
        exit(1)

    with open(shodan_json_file) as jsonFile, open(output, 'w') as fileToWrite:
        
        correctIpCounter = 0
        incorrectIpCounter = 0
        loopCounter = 0
        startTime = time.time()

        for line in jsonFile:
            jsonObject = json.loads(line)
            ip = jsonObject.get('ip_str')
            port = str(jsonObject.get('port'))
            size = jsonObject.get('total_data_set_size_in_bytes')

            try:
                if ipaddress.ip_address(ip):
                    writableOutput = f'{ip}'
                    fileToWrite.write("%s\n" % writableOutput)
                    correctIpCounter += 1
            except ValueError:
                incorrectIpCounter += 1

if __name__ == '__main__':
    main()
