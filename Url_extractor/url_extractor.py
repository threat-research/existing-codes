import re
import sys

filepath = sys.argv[1]
write_file = (filepath.rsplit(".",1)[0])
with open(filepath,"r",encoding='utf8', errors='ignore') as file:
    data = file.read()

    urls = re.findall('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', data)
    with open(write_file+"EXTRACTED_URLS.txt", "w") as wf:
        for url in urls:
            wf.write(url+"\n")