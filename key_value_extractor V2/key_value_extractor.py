import re,os
import csv,sys, itertools
import json


#Put search keywords in the below list:
keyword_list = ["category","location"]
path_list = []

file_path = sys.argv[1]

if os.path.isfile(file_path):
    path_list.append(file_path)
elif os.path.isdir(file_path):
    for r,d,f in os.walk(file_path):
        for files in f:
            filepath = os.path.join(r,files)
            if os.path.isfile(filepath):
                path_list.append(filepath)
else:
    print("Invalid input file/folder path")

all_results = []
for keyword in keyword_list:
    matches = []
    for file in path_list:
        with open(file,'r',encoding="utf-8") as f:
            lines = f.read()
            index1 = re.findall(r'(\"\W{0,1}' + keyword + '\":\s{0,1}\".+?\")',lines)
            index2 = re.findall(r'(\"\W{0,1}' + keyword + '\":\s{0,1}\[.+?\])', lines)
            index3 = re.findall(r'(\"\W{0,1}' + keyword + '\":\s{0,1}\{.+?\})', lines)
            matches.extend(index1)
            matches.extend(index2)
            matches.extend(index3)
    # print(keyword," : ",matches)
    all_results.append(matches)

with open('Matched_keys.csv', 'w', encoding="utf-8") as file:
    all_results = itertools.zip_longest(*all_results, fillvalue = '')
    writer = csv.writer(file, delimiter=",", lineterminator="\n")
    writer.writerow(keyword_list)
    writer.writerows(all_results)

print("Values extracted and written to 'Matched_keys.csv' file successfully for keywords")