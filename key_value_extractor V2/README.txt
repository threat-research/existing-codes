key_value_extractor:

Use: Extracts all the values from single/multiple files for given keywords

Instructions : Script to search values for given keys from single/multiple database file
1. for searching a file, give filepath as argument, or
2. for searching on multiple files in a folder, give folder path as argument.

3. Add your keys to "keyword_list" inside key_value_extractor.py script
4. Script will search through single/multiple data files and write values for each key to separate columns in 'Matched_keys.csv' file

Requirements:
python 3.6 or above

Usage:python3 key_value_extractor.py "file/folder path"

Output:
CSV file for all searched keywords
