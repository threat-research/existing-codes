python version : 3.6 and above

Use: Extract database tables from multiple Mongo database IPs, writes data to .txt files for each table

Steps:

1.pip3 install pymongo
2.pip3 install pandas
3.Run script with argument as "file.txt" file containing IP names
4.RUN: python3 mongo_extractor.py ip_file.txt
5.Output will populate in current working directory

Note:
sample IP input file given in project folder