from pymongo import MongoClient,errors
from bson import json_util
import os,sys

cwd = os.getcwd()

ip_list = []
mongo_ips = sys.argv[1]

with open(mongo_ips, "r", encoding="utf-8") as read_file:
    for line in read_file.readlines():
        if not len(line)<11:
            line = line.replace(" ","").strip("\n")
            ip_list.append(line)

# connect to MongoDB
for ip in ip_list:
    print("\nIP : ",ip+"\n")

    try:
        # try to instantiate a client instance
        client = MongoClient(ip,
            serverSelectionTimeoutMS=3000  # 3 second timeout
        )

        # print the version of MongoDB server if connection successful
        print("server version:", client.server_info()["version"])

        if client != None:
            database_names = client.list_database_names()

        IP_folder = os.path.join(cwd,ip+"_mongodb")
        if not os.path.exists(IP_folder):
            os.mkdir(IP_folder)

        print("Total databases found for IP ",ip," = ",len(database_names))
        for db in database_names:
            collection_names = client[db].list_collection_names()
            #print(collection_names,"\n\n")
            for collection in collection_names:
                write_file = os.path.join(IP_folder,collection+".json")
                records = client[db][collection].find()
                count = 0
                with open(write_file, "w", encoding="utf-8") as wf:
                    for record in records:
                        if count<=19:
                            wf.write(json_util.dumps(record))
                            wf.write("\n")
                            count+=1
                print("JSON successfully written for {} file".format(collection))


    except errors.ServerSelectionTimeoutError as err:
        # set the client instance to 'None' if exception
        client = None

        # catch pymongo.errors.ServerSelectionTimeoutError
        print("pymongo ERROR:", err)
