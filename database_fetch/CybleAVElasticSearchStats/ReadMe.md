#### ElasticSearch Stats & Data Grabber

This Script Built Using Go version go1.15  returns details from a Elastic Search server susch as IndiceName,	DocumentCount,	StoreSize,	IndiceProperties (Object Map)
 and first 1000 Documents stored as json.

---
##### Pre-Requisites 

Install Go Lang 1.15  from : `https://golang.org/dl/`

---
##### How To Use 
 
 - open Terminal / CommandPropmt 
 - Change Directory To ElasticStats Script Directory
 - Run Command ```go build -o ElasticStats``` 
 
 Now the Script is ready to be used 
 
 - Run the Script using the following command 
        
        ./ElasticStats <IP Address> <Output Directory Of Your Choice>
        
  E.g
  
        ./ElasticStats  49.128.186.237 /home/av/Desktop/Elastic-Output/


  This command will get all the details about the ElasticSearch server running on the provided IP address
  and generate a main-results.csv file along with json files [one for each Indice with first 1000 records in JSON format].
  when there is a need of getting status of multiple IPs Run the above command with different IPs and let the output path be same 
  doing so will append the data related to server to main-results.csv and new files containing data from the server.
  
  
  ---
  ##### Understadnig The Output 
  
   The main-results.csv file will be created in the output directory path this CSV file Will Following the Following Structure
    
    ``` IP,	IndiceName,	DocumentCount,	StoreSize,	IndiceProperties (Object Map), Path To JSON File ```

    

