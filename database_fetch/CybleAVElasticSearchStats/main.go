package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
)

type Payload struct {
	Query Query `json:"query"`
}
type MatchAll struct {
}
type Query struct {
	MatchAll MatchAll `json:"match_all"`
}

func main()  {


	ip := os.Args[1]
	outputpath :=  os.Args[2]

	f, err := os.OpenFile(outputpath+"elastic-main-result.csv", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0600)
	if err != nil {
		fmt.Println(err)
		return
	}



	respIndices, err := http.Get("http://"+ip+":9200/_cat/indices")
	if err != nil {
		print(err)
	}

	defer respIndices.Body.Close()

	bodyIndices, err := ioutil.ReadAll(respIndices.Body)
	if err != nil {
		print(err)
	}

	indexesES := string(bodyIndices)

	lines := strings.Split(indexesES, "\n")


	for _, line := range lines {
		if line != "" {

			lineItem := strings.Fields(line)

			respIndicesStruct, err := http.Get("http://"+ip+":9200/"+lineItem[2]+"/_mapping")
			if err != nil {
				print(err)
			}

			defer respIndicesStruct.Body.Close()

			bodyIndicesStructure, err := ioutil.ReadAll(respIndicesStruct.Body)
			if err != nil {
				print(err)
			}

			var bodyIndicesStructureMap map[string]interface{}

			json.Unmarshal([]byte(bodyIndicesStructure), &bodyIndicesStructureMap)


			fmt.Println("Index Name: ",lineItem[2])

			fmt.Println(string(bodyIndicesStructure))
			//var bodyIndicesStructureMapProperties map[string]interface{}

			_, err = fmt.Fprint(f,ip,",",lineItem[2],",",lineItem[6],",",lineItem[8],",",bodyIndicesStructureMap,outputpath+ip+lineItem[2]+"-data.json","\n")
			if err != nil {
				fmt.Println(err)
				f.Close()
				return
			}

			fmt.Println(ip,",",lineItem[2],",",lineItem[6],",",lineItem[8],",",bodyIndicesStructureMap)

			w, err := os.OpenFile(outputpath+ip+lineItem[2]+"-data.json", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0600)
			if err != nil {
				fmt.Println(err)
				return
			}

			data := Payload{
				// fill struct
			}
			payloadBytes, err := json.Marshal(data)
			if err != nil {
				// handle err
			}
			body := bytes.NewReader(payloadBytes)

			req, err := http.NewRequest("GET", "http://"+ip+":9200/"+lineItem[2]+"/_search?size=1000", body)
			if err != nil {
				// handle err
			}
			req.Header.Set("Content-Type", "application/json")

			resp, err := http.DefaultClient.Do(req)
			if err != nil {
				// handle err
			}
			defer resp.Body.Close()

			respBodyData, err := ioutil.ReadAll(resp.Body)
			if err != nil {
				print(err)
			}

			strData := string(respBodyData)

			_, err = fmt.Fprint(w,strData,"\n")
			if err != nil {
				fmt.Println(err)
				f.Close()
				return
			}

		}
	}


}
