package main

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"strings"
	"time"

	"github.com/gocql/gocql"
)

func main() {

	ip := os.Args[1]
	output := os.Args[2]

	cluster := gocql.NewCluster(ip)
	cluster.ProtoVersion = 4
	cluster.Consistency = gocql.One
	cluster.ConnectTimeout = time.Second * 100
	session, err := cluster.CreateSession()
	if err != nil {
		log.Println(err)
		return
	}


	f, err := os.OpenFile(output+"main-results.csv", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0600)
	if err != nil {
		fmt.Println(err)
		return
	}

	iterKeySpaces := session.Query("SELECT * FROM system_schema.keyspaces").Iter()

	keySpaces := map[string]interface{}{}

	for iterKeySpaces.MapScan(keySpaces) {
		//fmt.Println(keySpaces["keyspace_name"])

		iterKeySpacesTables := session.Query("SELECT table_name FROM system_schema.tables WHERE keyspace_name = ?;",keySpaces["keyspace_name"]).Iter()

		tables := map[string]interface{}{}
		for iterKeySpacesTables.MapScan(tables) {

			// fmt.Println("--",tables["table_name"])
				keyN  := keySpaces["keyspace_name"].(string)
				tblN  := tables["table_name"].(string)


				columns := session.Query("SELECT column_name FROM system_schema.columns  WHERE keyspace_name = ?  AND table_name = ?;",keyN,tblN).Iter()
				columnNames := map[string]interface{}{}
				var cols []string
				for columns.MapScan(columnNames) {

					cols = append(cols,columnNames["column_name"].(string))
					columnNames = map[string]interface{}{}
				}


				count := session.Query("SELECT COUNT(*) FROM "+keyN+"."+tblN).Iter()
				// fmt.Println(count)
				countNo := map[string]interface{}{}

				for count.MapScan(countNo) {

						fmt.Println(ip,",",keySpaces["keyspace_name"],",",tables["table_name"],",",countNo["count"],", [",strings.Join(cols, "  "),"]")

						_, err := fmt.Fprint(f,ip,",",keySpaces["keyspace_name"],",",tables["table_name"],",",countNo["count"],", [",strings.Join(cols, "  "),"],"+output+keyN+"."+tblN+".json  \n")
						if err != nil {
							fmt.Println(err)
							f.Close()
							return
						}


					countNo = map[string]interface{}{}
				}

				dataFromSource := session.Query("SELECT * FROM "+keyN+"."+tblN+" LIMIT 1000").Iter()
				// fmt.Println(count)
				dataFromSourceMap := map[string]interface{}{}

				for dataFromSource.MapScan(dataFromSourceMap){

					jsonString, err := json.Marshal(dataFromSourceMap)
					fmt.Println(err)

					w, err := os.OpenFile(output+ip+keyN+"."+tblN+".json", os.O_CREATE|os.O_WRONLY, 0600)
					if err != nil {
						fmt.Println(err)
						return
					}

					jsonstr := string(jsonString)
					_, err = fmt.Fprint(w,jsonstr,"\n")
					if err != nil {
						fmt.Println(err)
						w.Close()
						return
					}

					fmt.Println(string(jsonString))

					w.Close()

				}


			tables = map[string]interface{}{}
		}

		keySpaces = map[string]interface{}{}


	}

	defer session.Close()
}

