#### Cassandra Stats & Data Grabber

This Script Built Using Go version go1.15  returns details from a casandra server susch as KeySpaces , Tables , No. of Row in the Tables , Table Structures and firt 1000 records stored as json.

---
##### Pre-Requisites 

Install Go Lang 1.15  from : `https://golang.org/dl/`

Post Installation run command : ```go get github.com/gocql/gocql```

---
##### How To Use 
 
 - open Terminal / CommandPropmt 
 - Change Directory To CassendraStats Script Directory
 - Run Command ```go build -o CassandraStats``` 
 
 Now the Script is ready to be used 
 
 - Run the Script using the following command 
        
        ./CassandraStats <IP Address> <Output Directory Of Your Choice>
        
  E.g
  
        ./CassandraStats 125.212.239.15 /home/av/Desktop/Cassandra-Output/

  This command will get all the details about the cassendra server running on the provided IP address
  and genrate a main-output.csv file along with json files [one for each table with first 1000 records in JSON format].
  when there is a need of getting status of multiple IPs Run the above command with diffent IPs and let the output path be same 
  doing so will append the data related to server to main-results.csv and new files containig data from the server.
  
  
  ---
  ##### Understadnig The Output 
  
   The main-results.csv file will be created in the output directory path this CSV file Will Follwow the Following Structure
    
    ``` IP Address, KeySpace Name, Table Name, No. of Records, Table Coloumns [], Path To JSON File ```

    


