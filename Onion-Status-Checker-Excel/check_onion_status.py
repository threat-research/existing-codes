import os, sys
import argparse
import subprocess
import xlrd, xlwt
from xlutils.copy import copy


# Initiate the parser
parser = argparse.ArgumentParser(description='Program to check status of .onion')
parser.add_argument("-EN", "--excelname", help="Put Excel file name")
parser.add_argument("-SN", "--sheetname", help="Put Workbook sheet name")
parser.add_argument("-OC", "--onioncell", help="Put cell number for onions")
parser.add_argument("-SC", "--statuscell", help="Put cell number for status")

# Read arguments from the command line
args = parser.parse_args()

if len(sys.argv[1:]) > 0:

	if args.excelname != None and args.sheetname != None and args.onioncell != None and args.statuscell != None:

		# Opening excel file
		rb = xlrd.open_workbook(args.excelname)
		# Copying it to another variable (later used for writing)
		wb = copy(rb)
		# Open the desired sheet
		worksheet = rb.sheet_by_name(args.sheetname)

		# Grabing & Saving the onion links to a file
		onion_index = {}
		with open('links.txt', 'w+') as f:
			for index in range(worksheet.nrows):
				value = worksheet.cell_value(index, int(args.onioncell))
				if value != xlrd.empty_cell.value and ('.onion' in value or 'http' in value):
					onion_index[index] = value.strip()
					f.write(value)
					f.write('\n')

		# Running Onioff script
		onioff = subprocess.run(['python3', 'onioff/onioff.py', '-f', 'links.txt', '-o', 'report.txt'], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		
		# Opening & Checking the report file for onions status
		with open('report.txt', 'r') as f:
			lines = f.readlines()

		# Updating the excel sheet

		for key, value in onion_index.items():
			match_index = [index for index, elem in enumerate(lines) if value.strip('/') in elem]
			if 'Onion Inactive' in lines[match_index[0]]:
				onion_index[key] = 'INACTIVE'
			else:
				onion_index[key] = 'ACTIVE'

		for key, value in onion_index.items():
			wb.get_sheet(args.sheetname).write(key, int(args.statuscell), value)

		wb.save(args.excelname)

		# Deleting report.txt for new run
		subprocess.run(['rm', 'report.txt'], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	
	else:
		print('Properly input for all the options')
		os._exit(1)

else:
	print('Use -h or --help for usage options')
