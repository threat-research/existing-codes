# Onion-Status-Checker-Excel

A simple tool to take an excel file and check the onion links for their up/down status and update the excel file with the status.

## Initial Setup

* Clone the repository using: 
 ```
 cd ~
 git clone https://github.com/apurvsinghgautam/Onion-Status-Checker-Excel
 ```
 
* Donload the required libraries:
 ```
 cd Onion-Status-Checker-Excel
 pip install -r requirements.txt
 cd onioff
 pip install -r requirements.txt
 ```
 
- Download tor using:
  ```
  sudo apt install tor
  ```

- Start tor using:
 ```
 sudo service tor start
 ```
 
- Check tor running status (should be active) using :
 ```
 service tor status
 ```
 
## Running the script

- Put your excel sheet inside the `Onion-Status-Checker-Excel` directory

- Run the script in the following way:
```
python3 check_onion_status.py -h                                                                                                 ─╯
usage: check_onion_status.py [-h] [-EN EXCELNAME] [-SN SHEETNAME] [-OC ONIONCELL] [-SC STATUSCELL]

Program to check status of .onion

optional arguments:
  -h, --help            show this help message and exit
  -EN EXCELNAME, --excelname EXCELNAME
                        Put Excel file name
  -SN SHEETNAME, --sheetname SHEETNAME
                        Put Workbook sheet name
  -OC ONIONCELL, --onioncell ONIONCELL
                        Put cell number for onions
  -SC STATUSCELL, --statuscell STATUSCELL
                        Put cell number for status

Example - python3 check_onion_status.py -EN "Link Tracker(Ransomware,Forums,Telegram).xlsx" -SN "Ransomware" -OC 1 -SC 3
```

Note - Cell numbers starts from 0

## Output
- The file will be overwritten with the status values

## Credits

- [Nikolaos Kamarinakis](nikolaskama.me) for his script [Onioff](https://github.com/k4m4/onioff) for checking the status of the onion links

## Contributor
- [Apurv Singh Gautam](https://github.com/apurvsinghgautam)
