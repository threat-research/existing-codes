Use: Scrap and download openly available data from S3 buckets

Requirements:

python 3.6 and above
Use pip to install following libraries:
1.requests
2.wget
3.Beautifulsoup4

Usage:
Give the directory path in arguments, where you want to download files

python3 S3_Scanner.py "directory_path"