import requests, lxml
from bs4 import BeautifulSoup
import sys, wget

output_dir = sys.argv[1]
urls = []
url_list = []
base_url = "http://inteliuspub.s3.amazonaws.com"


req = requests.get(base_url)

if req.status_code==200:
    soup = BeautifulSoup(req.content,features="xml")
    #print(req.content)
    urls = (soup.find_all("Key"))

    for i in urls:
        i = str(i).replace("<Key>","").replace("</Key>","")
        url_list.append(i)
    print("Total URLs found : ",len(url_list))
    print("Searching URLs with valid <200> response.....\n")

    for url in url_list:
        connect = base_url+"/"+url
        #print(connect)
        u_req = requests.get(connect,stream=True)

        if u_req.status_code == 200:
            print("\nDownloading : ", connect)
            try:
                wget.download(connect,output_dir)
                print("\n")
            except Exception as e:
                print("Error while downloading file= {}\nError = {}".format(connect,e))

else:
    print("connection error, response = ",req.status_code)

print("\n//-------------Code Execution Completed---------")