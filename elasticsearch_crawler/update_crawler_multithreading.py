#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul  8 00:32:18 2021

@author: ubuntu
"""

#!/usr/bin/python

import os
import sys
import glob
import time
import json
import os.path
import socket
import ast
from io import open
from elasticsearch import Elasticsearch
from queue import Queue
from threading import Thread


if sys.version_info[0] < 3:
    pVer = 2
else:
    pVer = 3

import requests
try:
    from nested_lookup import nested_lookup
except ImportError:
    print("Nested_lookup import not found.")
    if pVer == 3:
        print("please execute the command: pip3 install nested_lookup")
    else:
        print("please execute the command: pip install nested_lookup")

    sys.exit(1)

if pVer == 3:
    inpFunc = input
else:
    inpFunc = raw_input

size = 1000
pagesPerFile = 1000
scrollTimer = "1440"

# Take input for IP address, port, index, and values to save
if len(sys.argv) > 1:
    ipAdr = sys.argv[1]
else:
    ipAdr = inpFunc("IP address: ")
try:
    socket.inet_aton(ipAdr)
except socket.error:
    print("Invalid IP.")
    sys.exit()

if len(sys.argv) > 2:
    port = sys.argv[2]
else:
    port = inpFunc("Port (Default is 9200): ")
    if port == "":
        port = "9200"


if len(sys.argv) > 3:
    indices = []
    index = sys.argv[3:]
else:
    indices = []
    print("To list all indices go to http://{0}:{1}/_cat/indices?v (submit an empty line when finished):".format(ipAdr, port))
    indx = inpFunc("Value: ")
    while indx != "":
        if '[' in indx and ']' in indx:
            try:
                indices.append(ast.literal_eval(indx))
            except SyntaxError:
                print("Invalid input.")
        else:
            indices.append(indx)
        indx = inpFunc("Value: ")

class DownloadWorker(Thread):

    def __init__(self, queue):
        Thread.__init__(self)
        self.queue = queue

    def run(self):
        while True:
            # Get the work from the queue and expand the tuple
            index = self.queue.get()
            try:
                fetch_data(index)
            finally:
                self.queue.task_done()

        
def fetch_data(index):
    # Create session to keep track of cookies/headers
    s = requests.session()

    newScrollID = False

    # Creating directory for each index
    dir_name = './' + ipAdr + '-' + index + '/' + "temp.txt"

    # If there is a scrollID.txt file parse it to figure out where in the search we are
    if os.path.isfile('./' + ipAdr + '-' + index + '/' + ipAdr + "-scrollID.txt"):
        scrollFile = open(ipAdr + '-' + index + '/' + ipAdr + "-scrollID.txt", "r+", encoding="utf-8")
        scrollContents = scrollFile.read().split("\n")
        scrollFile.close()
        scrollID = scrollContents[0]

    else:
        newScrollID = True
        # If there is no scrollID.txt file
        # Send initial request to get a scrollID to start pulling all the data, and not just the 5000 results that you can get from a search

        # scrollContents contains the values we need to "scoll" through all the pages of results
        scrollContents = []
        r = s.post("http://" + ipAdr + ":" + port + "/" + index + "/_search?scroll=" + scrollTimer + "m&size=" + str(size), headers={'Content-Type': 'application/json'})
        #print("http://" + ipAdr + ":" + port + "/" + index + "/_search?scroll=" + scrollTimer + "m&size=" + str(size))
        if not r.ok:
            #print(r.text)
            print("Response not okay, exiting")
            #print(r.text)
            sys.exit(1)

        rJson = json.loads(r.text)

        if 'error' in rJson:
            print("The server returned an error")
            #print(rJson)
            sys.exit(1)

        scrollID = rJson["_scroll_id"]
        if type(rJson["hits"]["total"]) is not dict:
            totalRequests = str(int((rJson["hits"]["total"])/size))
        else:
            totalRequests = str(int((rJson["hits"]["total"]["value"])/size))

        scrollContents.append(scrollID)
        scrollContents.append(totalRequests)
        scrollContents.append("1")


    # Strip all whitespace from the scrollContents
    #print(str(scrollContents))
    for i in range(len(scrollContents)-1):
        scrollContents[i] = scrollContents[i].strip()

    # Create scroll files. We save 1000 "pages" of results per file
    #fileName = ipAdr + '-' + index + '_' + ipAdr + "-" + index + "-" + str(int(int(scrollContents[2]) / pagesPerFile)) + ".txt"
    fileName =  ipAdr + "-" + index + "-" + str(int(int(scrollContents[2]) / pagesPerFile)) + ".txt"

    f = open(fileName, "a", encoding='utf-16')

    if newScrollID:
        # Run each result through the parsing function
        for hit in rJson["hits"]["hits"]:
            cwd = hit["_source"]
            #csv = parse_single(cwd)
            print(cwd)
            f.write(u"%s\n" %cwd)

    # Loop through every request, get the results, parse them, and save them to their respective files
    while True:
        #print("Getting page %s / %s" %(scrollContents[2], scrollContents[1]))
        scrollContents[2] = str(int(scrollContents[2]) + 1)

        if int(scrollContents[1]) % pagesPerFile == 0:
            # If we've hit the 1000 pages per file for our scolling, save the file and open the next
            f.close()

            #fileName = ipAdr + '-' + index + '/' + ipAdr + "-" + index + "-" + str(int(int(scrollContents[2]) % pagesPerFile)) + ".txt"
            fileName = ipAdr + "-" + index + "-" + str(int(int(scrollContents[2]) % pagesPerFile)) + ".txt"
            f = open(fileName, "a", encoding='utf-16')

        # Get next "page" storia_moments
        #print("http://" + ipAdr + ":" + str(port) + "/_search/scroll?scroll=" + scrollTimer + "m&scroll_id=" + scrollID)
        r = s.post("http://" + ipAdr + ":" + str(port) + "/_search/scroll?scroll=" + scrollTimer + "m&scroll_id=" + scrollID, headers={'Content-Type': 'application/json'})
        if not r.ok:
            # This shouldn't happen often unless we're being ratelimited
            #print("Response not okay, sleeping 10 seconds")
            #print(r.text)
            #print("http://" + ipAdr + ":" + str(port) + "/_search/scroll?scroll=" + scrollTimer + "m&scroll_id=" + scrollID)
            time.sleep(10)
            continue

        # Update scrollID
        rJson = json.loads(r.text)
        scrollID = rJson["_scroll_id"]
        if scrollID != scrollContents[0]:
            scrollContents[0] = scrollID

        # Update scrollID.txt file if anything's changed
        scrollFile = open(ipAdr + "-scrollID.txt", "w",encoding='utf-8')
        for i in scrollContents:
            scrollFile.write(u"%s\n" %i)
        scrollFile.close()

        # If we're out of results, we've scraped everything
        #print(rJson)
        if len(rJson["hits"]["hits"]) == 0:
            #print(r.text)
            print("Got all data")
            f.close()
            break

        # Run each result through the parsing function
        for hit in rJson["hits"]["hits"]:
            cwd = hit["_source"]
            #csv = parse_single(cwd)
            print(cwd)
            f.write(u"%s\n" %cwd)
        time.sleep(1)


# Getting indices from DB
es = Elasticsearch([ipAdr],port=9200)
indices=es.indices.get_alias().keys()


queue = Queue()

# Number of threads according to availablity
no_of_threads = 2

for x in range(no_of_threads):
    worker = DownloadWorker(queue)
    # Setting daemon to True will let the main thread exit even though the workers are blocking
    worker.daemon = True
    worker.start()


# Put the tasks into the queue as a tuple
for index in indices:
    queue.put((index))


# Main thread to wait for the queue to finish processing all the tasks
queue.join()
    

