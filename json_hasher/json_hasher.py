#!/usr/bin/env python
# coding: utf-8

# In[7]:

import hmac
import sys,json
import base64

def keyed_hashing_algorithm(value):
    value_string = str(value).lower().strip()
    value_bytes = value_string.encode('utf-8')
    key = base64.b64decode("IZ4Y4O/TaYseyITkk15sVKQu+Y2af3TJNyOzU7DRMB4=")
    hash_bytes = hmac.digest(key, value_bytes, 'sha256')
    hash_string = base64.standard_b64encode(hash_bytes).decode('utf-8')
    return hash_string

def update(d,key,x):
    m = key.split(' ')
    if len(m) <= 3 and m[-2].isdigit():
        if type(d[m[0]]) is list:
            d[m[0]][int(m[-2])] = keyed_hashing_algorithm(x)
            
    else:
        for i in m[:-2]:
            if i.isdigit():
                d = d[int(i)]
            else:
                d = d[i]
        d[m[-2]] = keyed_hashing_algorithm(x)
    return d
    
def hash_dict(dict_):
    def hash_(x,key = ''):
        if type(x) is dict:
            for a in x:
                hash_(x[a],key +a+" ")
        elif type(x) is list:
            i = 0
            for a in x:
                hash_(a,key+str(i)+" ")
                i += 1
        else:
            m = update(dict_,key,x)
            return m
    hash_(dict_)
    return dict_
    
file = sys.argv[1]
write_file = file.split(".txt")

l=[]
with open(file,"r") as file:
    for i in file:
        x = json.loads(i)
        l.append(x)

final = []
for x in l:
    j = hash_dict(x)
    final.append(j)

#save to txt
with open(write_file[0]+"_hashed.txt", 'w') as f:
    for item in final:
        f.write("%s\n" % item)
#final  


# In[ ]:




