Instructions for hashing ".txt" files containing multiple JSON objects:

1. A sample file "profiles.txt" is given in project folder for testing.
2. "CYBLE- Keyed Hashing for Public Unsecured Datasets (1).docx" for understanding the hashing project.

Python Version:
3.7 and above

Libraries:
Standard Libraries (No need to install any libraries)

USAGE:
Python3 json_hasher.py "input_filepath"

RESULT:
A hashed file will generate in the same folder as input file with name "input_filepath_hashed.txt"