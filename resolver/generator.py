import sys,os,re

completelist = open("complete_list.txt", 'a')

brandlist = open ("brands.txt", 'r').readlines()
keywordslist = open ("keywords.txt", 'r').readlines()
tldlist = open ("tldlist.txt", 'r').readlines()

print ("[INFO] Loaded Brands List: " + str(len(brandlist)))
print ("[INFO] Loaded Keywords List: " + str(len(keywordslist)))
print ("[INFO] Loaded TLDs List: " + str(len(tldlist)))

brandcount = int(len(brandlist))
totaldomains=0
count =0
for brand in brandlist:
	brand=brand[:-1]
	localdomain = 0
	for keyword in keywordslist:
		keyword = keyword[:-1]
		for tld in tldlist:
			tld=tld[:-1]
			domain1 = str(brand)+str(keyword)+str(".")+str(tld)
			domain2 = str(keyword)+str(brand)+str(".")+str(tld)
			domain3 = str(brand)+"-"+str(keyword)+str(".")+str(tld)
			domain4 = str(keyword)+"-"+str(brand)+str(".")+str(tld)
			localdomain = localdomain +4
			value= str(domain1)+"\n"+str(domain2)+"\n"+str(domain3)+"\n"+str(domain4)+"\n"
			completelist.write (value)
	print ("[INFO] Written Combinations for: "+ brand)
	print ("[INFO] Above Brand Domains Written "+ str(localdomain))
	totaldomains = totaldomains + localdomain
	count = count+1
	print ("[INFO] Remaining Brands: "+ str(int(brandcount-count)))
	print ("[INFO] Total Domains Written: "+ str(totaldomains))
	print (" --------------------------------"+ "\n")

