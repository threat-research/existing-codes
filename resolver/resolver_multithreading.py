import sys,re
import requests
import csv,time,os
import time
import dns
#import concurrent.futures
import mmap
from queue import Queue
from threading import Thread


# Number of threads
no_of_threads = 4 


with open("complete_list.txt", "r+b") as f:
    mm = mmap.mmap(f.fileno(), 0)


start_time = time.time()
resolved_domain = open("resolved-domains.txt", 'w')


class DownloadWorker(Thread):

    def __init__(self, queue):
        Thread.__init__(self)
        self.queue = queue

    def run(self):
        while True:
            # Get the work from the queue and expand the tuple
            domain = self.queue.get()
            try:
                domain_resolver(domain)
            finally:
                self.queue.task_done()


def domain_resolver(domain):
    domain = domain.decode("utf-8")[:-1]
    #print ("[INFO] Checking for: " + domain)
    try:
        result = dns.resolver.query(domain, 'A')
        for val in result:
            print ("[INFO] Domain Resolved for: " +str(domain))
            print('A Record : ', str(val.to_text()))
            print ("-----------------------------")
            value = str(domain)+":"+ str(val.to_text()+ "\n")
            resolved_domain.write(value)
            resolved_domain.flush()
    except:
        pass


queue = Queue()


for x in range(no_of_threads):
    worker = DownloadWorker(queue)
    # Setting daemon to True will let the main thread exit even though the workers are blocking
    worker.daemon = True
    worker.start()


# Put the tasks into the queue as a tuple
for domain in iter(mm.readline,b""):
    queue.put((domain))


# Main thread to wait for the queue to finish processing all the tasks
queue.join()


print("--- %s seconds ---" % (time.time() - start_time))

