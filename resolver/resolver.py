import sys,re
import requests
import csv,time,os
import time
import dns.resolver

domains = open ("complete_list.txt", 'r').readlines()
start_time = time.time()
resolved_domain = open("resolved-domains.txt", 'a')

for domain in domains:
	domain=str(domain[:-1])
	#print ("[INFO] Checking for: " +str(domain))
	try:
		result = dns.resolver.query(domain, 'A')
		for val in result:
			print ("[INFO] Domain Resolved for: " +str(domain))
			print('A Record : ', str(val.to_text()))
			print ("-----------------------------")
			value = str(domain)+":"+ str(val.to_text()+ "\n")
			resolved_domain.write(value)
			resolved_domain.flush()

	except:
		pass

print("--- %s seconds ---" % (time.time() - start_time))
