import time
import schedule
import twint
from datetime import datetime, timedelta

#yesterday = datetime.now()-timedelta(1)
#yesterday = datetime.strftime(yesterday, '%Y-%m-%d')



keyword_list = ["Microsoft Exchange Server","#EarthEmpusa","#EvilEye","Malware","dataleak","hacked","#Databreach","#Dataleak","#Volvalex","#Revil","#Ransom"]
#list = ["Malware","dataleak","hacked","#Databreach","#Dataleak","#Volvalex","#Revil","#Ransom"]

# you can change the name of each "job" after "def" if you'd like.
def keyword_search():
	now = datetime.now()
	now_minus_hr = now - timedelta(minutes=60)
	now_minus_hr = datetime.strftime(now_minus_hr, "%Y-%m-%d %H:%M:%S")

	print ("Fetching Tweets For Keywords")
	c = twint.Config()
	# choose beginning time (narrow results)
	c.Since = str(now_minus_hr)
	# set limit on total tweets
	c.Limit = 1000
	# no idea, but makes the csv format properly
	c.Store_csv = True
	# choose username (optional)
	#c.Username = "insert username here"
	# choose search term (optional)
	# format of the csv
	# c.Custom = ["date", "time", "username", "tweet", "link", "likes", "retweets", "replies", "mentions", "hashtags"]
	for keywords in keyword_list:
		print("Scrapping twitter for keyword : {}\n----------********-----------\n".format(keywords))
		c.Search = keywords
		# change the name of the csv file
		c.Output = "filename_"+keywords+".csv"
		twint.run.Search(c)

# run once when you start the program

keyword_search()
#jobtwo()

# run every minute(s), hour, day at, day of the week, day of the week and time. Use "#" to block out which ones you don't want to use.  Remove it to active. Also, replace "jobone" and "jobtwo" with your new function names (if applicable)

schedule.every(60).minutes.do(keyword_search)
#schedule.every().day.do(keyword_search)
# schedule.every().day.at("10:30").do(jobone)
# schedule.every().monday.do(jobone)
# schedule.every().wednesday.at("13:15").do(jobone)

# schedule.every(1).minutes.do(jobtwo)
#schedule.every().hour.do(jobtwo)
# schedule.every().day.at("10:30").do(jobtwo)
# schedule.every().monday.do(jobtwo)
# schedule.every().wednesday.at("13:15").do(jobtwo)

while True:
  schedule.run_pending()
  time.sleep(1)
