#install pysocks and bs4
import socks
import sys, search_engines
import re
import socket
import requests
from bs4 import BeautifulSoup
from urllib.request import urlopen
import csv
#import numpy as np
socks.set_default_proxy(socks.SOCKS5, "localhost", 9150)
socket.socket = socks.socksocket
def getaddrinfo(*args):
    return [(socket.AF_INET, socket.SOCK_STREAM, 6, '', (args[0], args[1]))]

socket.getaddrinfo = getaddrinfo
page_offset=0
links_dict = {}
limit = "n"
print ("[+] Collecting Links..Please be patient..")
try:
	for keyword in search_engines.search_keywords:
		print("\n--------KEYWORD : {}---------\n".format(keyword))
		onion = []
		for key, value in search_engines.search_engines.items():
			keyword.replace(" ","+")
			if key == "Tor Search":
				res = requests.get(value+str(keyword)+"&ia=web")
			else:
				res = requests.get(value+str(keyword))

			if res.status_code == 200:
				print("------Connection successful for {}-------".format(key))
				soup = BeautifulSoup(res.content, 'html.parser')
				#print(soup.prettify())
				#soup.title
				if re.findall ('did not match any documents', str(res.content)):
					limit="y"
				else:
					count = len(onion)
					links = [link.get('href') for link in soup.find_all('a')]
					links = list(filter(None, links))
					for l in links:
						if "onion" in l:
							onion.append(l)
					print("Total Onion links found in {} : {}".format(key,len(onion)-count))

					#print(url)
					#page_offset = page_offset+10
					#print ("\t[-] Proceeding to the page: "+str(int(page_offset/10)))
				
			else:
				print("******Connection Unsuccessful for {}, Response status : {}*******".format(key,res.status_code))
			onion = list(set(onion))
			links_dict[keyword] = onion
except Exception as e:
	print("+++++++Exception occured in {}++++++++".format(key),"\nError : ", e)
	pass
#onion = pd.unique(onion).tolist()

#print(links_dict)


for key, value in links_dict.items():
	print ("\n\n[INFO] Collected Onion Links for {} : {} ".format(key,str(len(onion))))
	print('Scrapping {} and Saving In file...\n'.format(key))
	with open(str(key)+"_scrapped.csv","w",encoding='UTF-8') as f:
		writer = csv.writer(f,delimiter=",",lineterminator="\n")
		writer.writerow(['Link','Text'])
		for line in value:
			try:
				print ("[-] Scrapping URL: "+str(line))
				res_line = requests.get(line)
				soup_line = BeautifulSoup(res_line.content, 'html.parser')
				cleantext = BeautifulSoup(res_line.content, "lxml").text
				writer.writerow([line, cleantext])
			except:
				pass

	print('\n\n-------{} keyword Scrapped and Saved Successfully.--------\n\n'.format(key))