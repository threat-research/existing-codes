USE: Searches through multiple darkweb search engines, multiple keywords can be searched at one time using below instructions.

Usage: Python3 Torer.py
An outfile csv file named with "keyword"_scrapped.csv for all keywords will be created

Requirements:

1. Python 3
2. If not already installed, install these libraries:
pip3 install beautifulsoup4
pip3 install PySocks
3. Keep search_engines.py in the same folder as Torer.py script
4. Tor Browser should be open in background for the script to work

Note:

Adding search keywords to search_engines.py file
1. Add new keyword as string to search_keywords list inside search_engines.py file

Adding new search engines to search_engines.py file
1. Add a key value pair to the search_engines dictionary inside search_engines.py file
2. Key is "Search engine name" & Value is "search engine url" 

for example: 
Adding keyword "facebook" & "fb"
search_keywords = ["facebook", "fb"]

Adding google search 
search_engines = {"google search":"https://www.google.com/search?q="}