Requirements:
pandas library
Run on cmd/terminal: pip3 install pandas

Instructions : Script to search strings from single/multiple database file
1. for searching a file, give filepath as argument, or
2. for searching on multiple files in a folder, give folder path as argument.

3. Add your keywords to "search_keywords.txt" file and keep it in the same folder as string_searcher.py script
4.Add all the strings to search line by line in the search_keywords.txt

5. Script will search through single/multiple DBs and write matches for each keyword to separate file "output_'keyword'.txt"

Usage:
python string_searcher.py "database_path"