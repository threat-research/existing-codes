import sys
import os,csv
import pandas as pd

'''---Add keywords to search in below list---
search_keywords = ["key", "socialId"]'''

cwd = os.getcwd()
db_name = sys.argv[1]
db_list = []

if os.path.isfile(db_name):
    db_list.append(db_name)
elif os.path.isdir(db_name):
    for r,d,f in os.walk(db_name):
        for files in f:
            filepath = os.path.join(r,files)
            if os.path.isfile(filepath):
                db_list.append(filepath)
else:
    print("Invalid input file/folder path")

with open("search_keywords.txt", "r") as file:
    for keyword in file.readlines():
        keyword = keyword.rstrip("\n")
        csv_name = 'output_' + str(keyword) + '.csv'

        #clearing old files
        if os.path.isfile(csv_name):
            os.remove(csv_name)

        for files in db_list:
            filename = os.path.basename(files)

            if not (files.endswith(".xlsx") or files.endswith(".xls")):
                #code for files except Excel
                with open(csv_name, 'a', encoding="utf-8") as file:
                    file.writelines("FILENAME : "+ filename+"\n\n")
                    with open(files, "r", encoding="utf-8",errors='ignore') as read_file:
                        for line in read_file:
                            if str(keyword) in line:
                                file.writelines(line)
            else:
                #Code For Excel Files
                df = pd.read_excel(files)
                with open(csv_name, 'a', encoding="utf-8") as file:
                    csvwriter = csv.writer(file)
                    csvwriter.writerow(["FILENAME : "+filename])
                    for row in df.iterrows():
                        if keyword in (str(row[1])):
                            csvwriter.writerow(row[1].values)


