import re
import json
import sys
import os

cwd = os.getcwd()

db_name = sys.argv[1]
keyword = sys.argv[2]
match_list = []
match_dict = {}

with open(db_name, "r", encoding="utf-8") as read_file:
    for line in read_file.readlines():
        if str(keyword) in line:
            match_list.append(line)
#print(match_list)
match_dict.update(keyword=match_list)

with open((db_name+"_matched_data.json"),"w") as write_file:
    write_file.write(json.dumps(match_dict,
    sort_keys=True,
    indent=4,
    separators=(',', ': ')
))

