import sys
import os,sys

import pandas as pd

df = pd.DataFrame()
xlfname = sys.argv[1]

xl = pd.ExcelFile(xlfname)

for sheet in xl.sheet_names:
    df_tmp = xl.parse(sheet)
    df = df.append(df_tmp, ignore_index=True,sort=False)
    # For creating different csv file for each sheet
    #csvfile = sheet+'.csv'
    #df.to_csv(csvfile, index=False)

csvfile = os.path.splitext(xlfname)[0]+'.csv'
df.to_csv(csvfile, index=False)

