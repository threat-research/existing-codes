import pandas as pd
import csv
import openpyxl
import os,sys

folder_path = sys.argv[1]

f_paths = []
for r,d,f in os.walk(folder_path):
    for filenames in f:
        print(filenames)
        if ".xlsx" in filenames:
            f_paths.append(os.path.join(r,filenames))
print(f_paths)
counter = 0
for f in f_paths:
    a= f.rstrip(".xlsx")+".csv"
    print(a)
    excel = openpyxl.load_workbook(f)
    sheet = excel.active
    # writer object is created
    col = csv.writer(open(a,'w',newline="",encoding='utf8'))

    # writing the data in csv file
    for r in sheet.rows:
        # row by row write
        # operation is perform
        col.writerow([cell.value for cell in r])
    #excel.close()
    print(counter)
    counter += 1


