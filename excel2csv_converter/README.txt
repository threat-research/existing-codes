USE: converts xlsx data files to csv files

Requirements:
Python 3.6 or above
Libraries:
Install these libraries if not already installed
pip3 install openpyxl
pip3 install pandas

Usage: 
python3 xlsx2csv.py "input_folder_path"