#install pysocks and bs4
import socks,sys,re
import socket
import requests
from bs4 import BeautifulSoup
from urllib.request import urlopen
import csv
#import numpy as np

socks.set_default_proxy(socks.SOCKS5, "localhost", 9150)
socket.socket = socks.socksocket
def getaddrinfo(*args):
    return [(socket.AF_INET, socket.SOCK_STREAM, 6, '', (args[0], args[1]))]

socket.getaddrinfo = getaddrinfo
url=sys.argv[1]
page_offset=0
onion = []
limit = "n"
print ("[+] Collecting Links..Please be patient..")
while (limit == "n"):
	try:
		res = requests.get("http://quosl6t6c64mnn7d.onion/results?&q="+str(sys.argv[1])+"&offset="+str(page_offset))
		soup = BeautifulSoup(res.content, 'html.parser')
		#print(soup.prettify())
		#soup.title
		if re.findall ('did not match any documents', str(res.content)):
			limit="y"
		else:
			links = [link.get('href') for link in soup.find_all('a')]
			links = list(filter(None, links))
			for l in links:
				if "onion" in l:
					onion.append(l)
		page_offset = page_offset+10
		print ("\t[-] Proceeding to the page: "+str(int(page_offset/10)))
	except:
		pass
#onion = pd.unique(onion).tolist()



onion = list(set(onion))

print ("[INFO] Collected Onion Links: "+str(len(onion)))

print('Scrapping and Saving In file...')

with open("scrapped.csv","w",encoding='UTF-8') as f:
	writer = csv.writer(f,delimiter=",",lineterminator="\n")
	writer.writerow(['Link','Text'])
	for line in onion:
		try:
			print ("[-] Scrapping URL: "+str(line))
			res_line = requests.get(line)
			soup_line = BeautifulSoup(res_line.content, 'html.parser')
			cleantext = BeautifulSoup(res_line.content, "lxml").text
			writer.writerow([line, cleantext])
		except:
			pass

print('Scrapped and Saved Successfully.')