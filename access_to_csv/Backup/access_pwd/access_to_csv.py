import os
import pyodbc
import csv
import re
import sys

# MS ACCESS DB CONNECTION
pyodbc.lowercase = False


def tables(table_name, conn, db_file):
    try:
        cur1 = conn.cursor()
        query = ("SELECT * FROM " + "\"" + table_name + "\"")
        cur1.execute(query)
        output_csv = os.path.splitext(db_file)[0]
        output_csv = output_csv + "-" + table_name
        with open(output_csv + '.csv', 'a', newline='', encoding="utf-8") as f:
            for line in cur1.fetchall():
                writer = csv.writer(f)
                writer.writerow(line)
        f.close()
        cur1.close()
    except Exception as e:
        if not str(e).find("Too few parameters.") == -1:
            pass
        else:
            print("Error at def tables : " + str(e))


def get_database(db_file):
    try:
        if len(sys.argv) == 1:
            conn = pyodbc.connect(
                r"Driver={Microsoft Access Driver (*.mdb, *.accdb)};" +
                r"Dbq=" + db_file + ";")
        else:
            conn = pyodbc.connect(
                r"Driver={Microsoft Access Driver (*.mdb, *.accdb)};" +
                r"Dbq=" + db_file + ";" +
                r"PWD=" + sys.argv[1] + ";")

        cur = conn.cursor()
        db_list = list(cur.tables())
        cur.close()
        for row in db_list:
            if not row.table_name.startswith('MSys'):
                tables(str(row.table_name), conn, db_file)

        conn.close()

    except Exception as e:
        if not str(e).find("Not a valid password") == -1:
            print("Please enter password in argument for file : " + db_file)
        else:
            print("Error at def get_database : " + str(e))


def main():
    try:
        directory = os.getcwd()
        for subdir, dirs, files in os.walk(directory):
            for filename in files:
                if filename.endswith(".mdb") or filename.endswith(".accdb"):
                    file = os.path.join(subdir, filename)
                    # print("Converting : " + file + " to csv.")
                    get_database(file)
                    # print("Converted : " + file + " to csv.")
            else:
                continue
    except Exception as e:
        print("Error at def main : " + str(e))


if __name__ == "__main__":
    main()
