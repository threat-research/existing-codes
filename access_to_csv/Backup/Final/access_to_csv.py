import pyodbc
import csv
import re
import sys

# MS ACCESS DB CONNECTION
pyodbc.lowercase = False

Dbq = sys.argv[1]
output = sys.argv[2]
print(Dbq)
conn = pyodbc.connect(
    r"Driver={Microsoft Access Driver (*.mdb, *.accdb)};" +
    r"Dbq=" + Dbq + ";")


def tables(table_name):
    cur1 = conn.cursor()
    query = ("SELECT * FROM " + "\"" + table_name + "\"")
    cur1.execute(query)
    with open(output + '.csv', 'a', newline='') as f:
        for line in cur1.fetchall():
            writer = csv.writer(f)
            writer.writerow(line)
    f.close()
    cur1.close()
    print("Data from table "+table_name+" is fetched.")


cur = conn.cursor()
print("Database is connected.")
list = list(cur.tables())
cur.close()
for row in list:

    if not row.table_name.startswith('MSys'):
        print("Fetching data from " + str(row.table_name) + ".")
        tables(str(row.table_name))
print("Database is converted to csv.")
conn.close()
