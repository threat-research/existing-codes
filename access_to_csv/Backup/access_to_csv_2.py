import csv, pyodbc
import os,sys

# set up some constants
#MDB = 'c:/path/to/my.mdb'
MDB = sys.argv[1]
DRV = '{Microsoft Access Driver (*.mdb,*.accdb)}'
PWD = 'pw'
1
# connect to db
con = pyodbc.connect('DRIVER={};DBQ={};PWD={}'.format(DRV,MDB,PWD))
cur = con.cursor()

# run a query and get the results
SQL = 'SELECT * FROM mytable;' # your query goes here
rows = cur.execute(SQL).fetchall()
cur.close()
con.close()

# you could change the mode from 'w' to 'a' (append) for any subsequent queries
with open(os.path.splitext(MDB)[0]+'.csv', 'w') as fou:
    csv_writer = csv.writer(fou) # default field-delimiter is ","
    csv_writer.writerows(rows)