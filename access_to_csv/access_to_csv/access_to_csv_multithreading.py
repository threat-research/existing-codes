import os
import pyodbc
import csv
import re
import sys
from queue import Queue
from threading import Thread

# MS ACCESS DB CONNECTION
pyodbc.lowercase = False

def init(init_db_file):
    if len(sys.argv) == 1:
        connection = pyodbc.connect(
        r"Driver={Microsoft Access Driver (*.mdb, *.accdb)};" +
        r"Dbq=" + init_db_file + ";")
        
    else:
        connection = pyodbc.connect(
        r"Driver={Microsoft Access Driver (*.mdb, *.accdb)};" +
        r"Dbq=" + init_db_file + ";" +
        r"PWD=" + sys.argv[1] + ";")
    return connection


class DownloadWorker(Thread):

    def __init__(self, queue):
        Thread.__init__(self)
        self.queue = queue

    def run(self):
        while True:
            # Get the work from the queue and expand the tuple
            table_name, db_file = self.queue.get()
            
            try:
                tables(table_name, db_file)
            finally:
                self.queue.task_done()


def tables(table_name, db_file):
    try:
        conn = init(db_file)
        cur1 = conn.cursor()
        query = ("SELECT * FROM " + "\"" + table_name + "\"")
        cur1.execute(query)
        output_csv = os.path.splitext(db_file)[0] + "_" + table_name
        with open(output_csv + '.csv', 'a', newline='', encoding="utf-8") as f:
            for line in cur1.fetchall():
                writer = csv.writer(f)
                writer.writerow(line)
        f.close()
        cur1.close()
    except Exception as e:
        print("Error at def table : " + str(e))


def get_database(db_file):
    try:
        queue = Queue()
        '''if len(sys.argv) == 1:
            conn = pyodbc.connect(
                r"Driver={Microsoft Access Driver (*.mdb, *.accdb)};" +
                r"Dbq=" + db_file + ";")
        else:
            conn = pyodbc.connect(
                r"Driver={Microsoft Access Driver (*.mdb, *.accdb)};" +
                r"Dbq=" + db_file + ";" +
                r"PWD=" + sys.argv[1] + ";")'''
        conn = init(db_file)
        cur = conn.cursor()
        db_list = list(cur.tables())
        cur.close()
        no_of_threads = 2
        for x in range(no_of_threads):
            worker = DownloadWorker(queue)
            # Setting daemon to True will let the main thread exit even though the workers are blocking
            worker.daemon = True
            worker.start()
        
        for row in db_list:
            if not row.table_name.startswith('MSys'):
                queue.put((str(row.table_name), db_file))
                
        conn.close()
        queue.join()
    except Exception as e:
        if not str(e).find("Not a valid password") == -1:
            print("Please enter password in argument for file : " + db_file)
        else:
            print("Error at def get_database : " + str(e))






def main():
    try:
        directory = os.getcwd()
        for subdir, dirs, files in os.walk(directory):
            for filename in files:
                if filename.endswith(".mdb") or filename.endswith(".accdb"):
                    file = os.path.join(subdir, filename)
                    # print("Converting : " + file + " to csv.")
                    get_database(file)
                    # print("Converted : " + file + " to csv.")
            else:
                continue
    except Exception as e:
        print("Error at def main : " + str(e))


if __name__ == "__main__":
    main()
