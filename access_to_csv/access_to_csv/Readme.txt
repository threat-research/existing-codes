Script is for converting access databases both .mdb and .accdb password protected and/or without password protection to csv file.
All the tables in the database will be merged into one single csv.

Supported Platform: Windows 10

Prerequisites: 
	1. MS Access application or Ms access database engine [https://www.microsoft.com/en-in/download/details.aspx?id=13255]
	2. pyodbc package from pypi
	3. Use console2 (Console is a Windows console window enhancement) instead of default cmd.exe otherwise it will result in character incompetence issues. [https://sourceforge.net/projects/console/]

To use teh script following steps needs to be taken:

	1. install pyodbc package using command "pip install pyodbc".
	2. Run the script in the folder where the database is present and password for the database as parameter. (password is optional)
	   "database_folder>python access_to_csv.py <password>"
	3. Output csv will have same name as database name and will be created at the database file location
	4. Script can iterate through subdirectories as well. 


Expected error: "Error at def table : ('07002', '[07002] [Microsoft][ODBC Microsoft Access Driver] Too few parameters. Expected 1. (-3010) (SQLExecDirectW)')"
because some tables are views which will have redundant data from the tables ignore the error as all data is fetched.
