#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec 22 05:28:05 2021

@author: Piyush Adhikar

"""
from bs4 import BeautifulSoup as bs
from urllib.parse import urlparse
import pandas as pd
from threading import Thread, active_count, enumerate
from queue import Queue
from time import sleep
import os, sys
from scrapper_utility import get_files
import re

parsed_urls=[]

hrefs = []

excluded = ['raidforums.com', 'rf.ws','dmca.com']
def parseDoc(filepath=None,excluded =None):
    
    if filepath:
        print(filepath)
        html_doc = open(filepath)
        soup = bs(html_doc, 'html.parser')                
        for link in soup.find_all('a', href = re.compile('^((https://)|(http://))')):
            if 'href' in link.attrs:                   
                if urlparse(link.attrs['href']).netloc in excluded:
                    continue
                else:
                    parsed_urls.append(link.attrs['href'])
                    #print(link.attrs['href'])

class ScrapeWorker(Thread):

    def __init__(self, queue):
        Thread.__init__(self)
        self.queue = queue

    def run(self):
        while True:
            # Get the work from the queue and expand the tuple
            file = self.queue.get()
            try:                
                parseDoc(file)
            finally:
                self.queue.task_done()

#root,dirs,files = list(os.walk("/home/ubuntu/Downloads/raidforums.com"))[0]

def scrapper():
    try:
        startcount = active_count()
        Q = Queue()
        threads = []
        for file in get_files(sys.argv[1]):
            threads.append(Thread(target=parseDoc, args=[file]))

        for thread in threads:
            thread.start()
            thread.join()
            while(active_count() - startcount > 2):
                sleep(5)

        Q.task_done()        
        
        while not Q.empty():
            df = Q.get()
            hrefs.append(df)
        
        Q.task_done()
                
            
    except Exception as e: print("Scrapper : " + str(e))
    
def data_processing(scrapped_url):
    try:                       
        known_url_pd = pd.read_csv("top-1m.csv",names=['Rank','Domain'])                        
        url_tld =[]
        new_list = []        
        for link in scrapped_url:            
            string = urlparse(link).netloc 
            string = string.replace("www.","")
            arr = string.rsplit('.')
            if len(arr)==2:
                url_tld.append(string)
                new_list.append([link, string])
            elif len(arr) == 3:                
                new_list.append([link,str(arr[-2]+"."+arr[-1])])
            elif len(arr) > 3:                
                new_list.append([link,str(arr[-3]+"."+arr[-2]+"."+arr[-1])])            
        df = pd.DataFrame(new_list, columns=["URL", "TL Domain"])                     
        python_only = df[df['TL Domain'].isin(known_url_pd['Domain']) == False]        
        for idx, row in python_only.iterrows():
            string = python_only.loc[idx,'TL Domain']            
            arr = string.rsplit('.')
            if  len(arr) == 3 :    
                python_only.at[idx,'TL Domain']= str(arr[-2]+"."+arr[-1])                        
        final = python_only[python_only['TL Domain'].isin(known_url_pd['Domain']) == False]                        
        g1 = final.groupby(["URL"], as_index=False).count()        
        g1 =g1.rename(columns ={"TL Domain":"Weight"})     
        print(g1)        
        g1.to_csv('output.csv')            
    except Exception as e: print(e)
    
def main():
    temp = scrapper()
    data_processing(temp)
        
    
    
if __name__=="__main__":
    main()


'''
queue = Queue()

# Number of threads according to availablity
no_of_threads = 2

for x in range(no_of_threads):
    worker = DownloadWorker(queue)
    # Setting daemon to True will let the main thread exit even though the workers are blocking
    worker.daemon = True
    worker.start()


# Put the tasks into the queue as a tuple
for file in get_files(sys.argv[1]):
    queue.put((file))
'''