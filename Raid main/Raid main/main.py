#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec 10 03:00:50 2021

@author: Piyush Adhikar
"""
import scrapy
import sys
from scrapy.crawler import CrawlerProcess
from scrapper_utility import get_files_1
from scrapy.linkextractors import LinkExtractor
from urllib.parse import urlparse
import pandas as pd
import time

parsed_urls=[]

class Raid_Forum(scrapy.Spider):
    name = 'Raid_Forum'
    start_urls = get_files_1(sys.argv[1])

    def parse(self, response):
        try:            
            excluded_domains = ['raidforums.com', 'rf.ws', 'dmca.com']
            extractor = LinkExtractor(deny_domains=excluded_domains)            
            links = extractor.extract_links(response)                        
            for link in links:
                if not "file:///" in link.url:                                       
                    parsed_urls.append(link.url)
            print("processing : "+response.url)
        except Exception as e: print(e)

def main():
    try:        
        url_tld =[]               
        process = CrawlerProcess()
        process.crawl(Raid_Forum)
        process.start()        
        known_url_pd = pd.read_csv("top-1m.csv",names=['Rank','Domain'])                        
        new_list = []
        for link in parsed_urls:            
            string = urlparse(link).netloc 
            string = string.replace("www.","")
            arr = string.rsplit('.')
            if len(arr)==2:
                url_tld.append(string)
                new_list.append([link, string])
            elif len(arr) == 3:                
                new_list.append([link,str(arr[-2]+"."+arr[-1])])
            elif len(arr) > 3:                
                new_list.append([link,str(arr[-3]+"."+arr[-2]+"."+arr[-1])])            
        df = pd.DataFrame(new_list, columns=["URL", "TL Domain"])                     
        python_only = df[df['TL Domain'].isin(known_url_pd['Domain']) == False]        
        for idx, row in python_only.iterrows():
            string = python_only.loc[idx,'TL Domain']            
            arr = string.rsplit('.')
            if  len(arr) == 3 :    
                python_only.at[idx,'TL Domain']= str(arr[-2]+"."+arr[-1])                        
        final = python_only[python_only['TL Domain'].isin(known_url_pd['Domain']) == False]                        
        g1 = final.groupby(["URL"], as_index=False).count()        
        g1 =g1.rename(columns ={"TL Domain":"Weight"})     
        print(g1)        
        g1.to_csv('output.csv')            
    except Exception as e: print(e)
    
if __name__=="__main__":
    start_time = time.time()
    main()
    print("--- %s seconds ---" % (time.time() - start_time))
    
