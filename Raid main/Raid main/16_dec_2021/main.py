#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec 10 03:00:50 2021

@author: ubuntu
"""
import scrapy
import sys
from scrapy.crawler import CrawlerProcess
from scrapper_utility import get_files
from scrapy.linkextractors import LinkExtractor
from urllib.parse import urlparse
import csv
import collections

parsed_urls=[]

class Raid_Forum(scrapy.Spider):
    name = 'Raid_Forum'
    start_urls = get_files(sys.argv[1])

    def parse(self, response):
        try:            
            excluded_domains = ['raidforums.com', 'rf.ws', 'dmca.com']
            extractor = LinkExtractor(deny_domains=excluded_domains)            
            links = extractor.extract_links(response)                        
            for link in links:
                if not "file:///" in link.url:
                    #if "onion" in link.url:
                    parsed_urls.append(urlparse(link.url).netloc)                   
                    #parsed_urls.append(link.url)
            print("Success : "+response.url)
        except Exception as e: print(e)

def main():
    
    known_url=[]
    
    file = open("top_10000.csv", "r")
    for lines in file:
        line = lines.strip()
        known_url.append(line)
    
    process = CrawlerProcess()
    process.crawl(Raid_Forum)
    process.start()
    
    #print(parsed_urls)
    #counter = collections.Counter(parsed_urls)
    #print(type(counter))
    #print("Count : "+len(counter))
    
    final_urls= list(set(parsed_urls)-set(known_url))
    file = open('final_urls.csv', 'w') 
    for link in final_urls: 
        file.write(link + '\n')       
    file.close()          
      
if __name__=="__main__":
    main()
    
