#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec 10 02:34:18 2021

@author: Piyush Adhikar
"""
import os


def get_files(path):
    if not path:
        path = os.getcwd()
    print(os.getcwd())

    file_list=[]
    for root, dirs, files in os.walk(path):
        for file in files:
            if file.endswith(".html") or file.endswith(".php"):
                full_path = os.path.join(path, file)
                file_list.append("file://"+full_path)
            
    return file_list