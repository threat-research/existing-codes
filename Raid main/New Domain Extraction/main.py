#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Completed on Fri Dec 24
@author: Piyush Adhikar
execution -
$python main.py /path/to/html/files

"""
import scrapy
import sys
from scrapy.crawler import CrawlerProcess
from scrapper_utility import get_files,domain_etraction
from scrapy.linkextractors import LinkExtractor
import pandas as pd


parsed_urls=[]

class Raid_Forum(scrapy.Spider):
    name = 'Raid_Forum'
    start_urls = get_files(sys.argv[1])

    def parse(self, response):
        try:            
            excluded_domains = ['raidforums.com', 'rf.ws', 'dmca.com']
            extractor = LinkExtractor(deny_domains=excluded_domains)            
            links = extractor.extract_links(response)                        
            for link in links:
                if not "file:///" in link.url:                                       
                    parsed_urls.append(link.url)
            print("processing : "+response.url)
        except Exception as e: print(e)

def main():
    try:        
        #================ Scrappy Process Starts =============================
        
        process = CrawlerProcess()
        process.crawl(Raid_Forum)
        process.start()                                              

        #================ Extract new URL ====================================
        
        url_tld_df = domain_etraction(parsed_urls)                 
        known_url_pd = pd.read_csv("top-1m.csv",names=['Rank','Domain']) # Load 1 million known domains        
        url_tld_df_1 = url_tld_df[url_tld_df['TL Domain'].isin(known_url_pd['Domain']) == False]                
        for idx, row in url_tld_df_1.iterrows():
            string = url_tld_df_1.loc[idx,'TL Domain']            
            arr = string.rsplit('.')
            if  len(arr) == 3 :    
                url_tld_df_1.at[idx,'TL Domain']= str(arr[-2]+"."+arr[-1])                                                
        url_tld_df_2 = url_tld_df_1[url_tld_df_1['TL Domain'].isin(known_url_pd['Domain']) == False]   

        #================ Compare with already extracted URLS ================
        
        extracted_url= pd.read_csv("extracted_urls.csv",names=['Extracted Links'])        
        extracted_url_list = extracted_url["Extracted Links"].tolist()
        for i in range(len(extracted_url_list)):
            if not "http" in extracted_url_list[i]:
                extracted_url_list[i]="http://"+extracted_url_list[i]              
        url_tld_df_3 = domain_etraction(extracted_url_list)                
        url_tld_df_4 = url_tld_df_2[url_tld_df_2['TL Domain'].isin(url_tld_df_3['TL Domain']) == False]

        #================ Final Output of the script ========================= 
        
        output = url_tld_df_4.groupby(["URL"], as_index=False).count()        
        output =output.rename(columns ={"TL Domain":"Weight"})                    
        print(output)
        output.to_csv('output.csv')            
    
    except Exception as e: print( "Main function exception : "+str(e))
    
if __name__=="__main__":    
    main()    
    
