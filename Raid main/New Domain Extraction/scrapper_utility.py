#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Completed on Fri Dec 24

@author: Piyush Adhikar
"""
import os
import pandas as pd
from urllib.parse import urlparse

#====================== Extract Files from folders ===========================
def get_files(path):
    try:
        if not path:
            path = os.getcwd()
    
        file_list=[]
        for root, dirs, files in os.walk(path):
            for file in files:
                if file.endswith(".html") or file.endswith(".php"):
                    full_path = os.path.join(path, file)
                    file_list.append("file://"+full_path)
                
        return file_list
    except Exception as e: print("get_files : "+str(e))
#=============================================================================

#====================== Extract TLD from the URL =============================
def domain_etraction(url_list):
    try:        
        url_tld =[]
        new_list = []        
        for link in url_list:                                              
                string = urlparse(link).netloc 
                arr = string.rsplit('.')                
                if len(arr)==2:
                    url_tld.append(string)
                    new_list.append([link, string])
                elif len(arr) == 3:                
                    new_list.append([link,str(arr[-2]+"."+arr[-1])])
                elif len(arr) > 3:                
                    new_list.append([link,str(arr[-3]+"."+arr[-2]+"."+arr[-1])])                    
        url_tld_df = pd.DataFrame(new_list, columns=["URL", "TL Domain"])        
        return url_tld_df
    except Exception as e: print("domain_etraction : "+str(e))
#=============================================================================